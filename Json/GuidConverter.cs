﻿using System;
using Newtonsoft.Json;

namespace NiuNai.Bottle.Json
{
    /// <summary>
    /// Guid转换。当字符串为空或者null时，返回Guid.Empty。
    /// </summary>
    public class GuidConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Guid);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var value = reader.Value as string;

            if (string.IsNullOrEmpty(value)) return Guid.Empty;

            switch (reader.TokenType)
            {
                case JsonToken.Null:
                    return Guid.Empty;
                case JsonToken.String:
                    return Guid.Parse(value);
                default:
                    throw new ArgumentException(string.Format("Unexpected token parsing Guid. Expected String, got {0}.", reader.TokenType));
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(value.ToString());
        }
    }
}