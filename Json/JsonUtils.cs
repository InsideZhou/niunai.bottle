﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NiuNai.Bottle.Lang;

namespace NiuNai.Bottle.Json
{
    /// <summary>
    ///     用于object与json字符串的序列化和反序列化。
    /// </summary>
    public class JsonUtils
    {
        private static JsonSerializerSettings _settings { get; set; }

        public static JsonSerializerSettings Settings
        {
            get { return _settings ?? GenerateSettings(); }
            set { _settings = value; }
        }

        public static JsonSerializerSettings GenerateSettings()
        {
            var settings = new JsonSerializerSettings();

            settings.MissingMemberHandling = MissingMemberHandling.Ignore;
            settings.PreserveReferencesHandling = PreserveReferencesHandling.None;
            settings.TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple;
            settings.TypeNameHandling = TypeNameHandling.None;
            settings.Formatting = Formatting.None;
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.DateFormatString = "yyyy-MM-dd HH:mm:ss.fff";

            settings.Converters.Add(new SimpleByteArrayConverter());
            settings.Converters.Add(new GuidConverter());

            return settings;
        }

        /// <summary>
        ///     将对象序列化为json字符串。
        /// </summary>
        public static string Serialize(object obj, JsonSerializerSettings settings = null)
        {
            if (null == obj) return null;

            return JsonConvert.SerializeObject(obj, settings ?? Settings);
        }

        /// <summary>
        ///     将json字符串反序列化为对象。
        /// </summary>
        /// <remarks>
        /// 当反序列化为DyObj时，Javascript Array生成为IList&lt;object&gt;；浮点数被解析为decimal类型。
        /// </remarks>
        public static T Deserialize<T>(string jsonStr, JsonSerializerSettings settings = null)
        {
            if (string.IsNullOrEmpty(jsonStr)) return default(T);

            var t = typeof(T);
            if (t == typeof(DyObj) || t == typeof(DyObj[]) || t == typeof(IList<DyObj>))
            {
                return (T) _parseDeserializedObject(JsonConvert.DeserializeObject(jsonStr, settings ?? Settings));
            }

            return JsonConvert.DeserializeObject<T>(jsonStr, settings ?? Settings);
        }

        /// <summary>
        ///     将json字符串反序列化为对象。
        /// </summary>
        /// <remarks>
        /// 当反序列化为DyObj时，Javascript Array生成为IList&lt;object&gt;；浮点数被解析为decimal类型。。
        /// </remarks>
        public static object Deserialize(Type type, string jsonStr, JsonSerializerSettings settings = null)
        {
            if (string.IsNullOrEmpty(jsonStr)) return null;

            if (type == typeof(DyObj) || type == typeof(DyObj[]) || type == typeof(IList<DyObj>))
            {
                return _parseDeserializedObject(JsonConvert.DeserializeObject(jsonStr, settings ?? Settings));
            }

            return JsonConvert.DeserializeObject(jsonStr, type, settings ?? Settings);
        }

        /// <summary>
        ///     将json字符串反序列化为匿名对象。
        /// </summary>
        public static T DeserializeAnonymous<T>(string jsonStr, T anonymousType, JsonSerializerSettings settings = null)
        {
            if (string.IsNullOrEmpty(jsonStr)) return default(T);

            return JsonConvert.DeserializeAnonymousType(jsonStr, anonymousType, settings ?? Settings);
        }

        private static object _parseDeserializedObject(object obj)
        {
            if (obj is JObject)
            {
                return _ParseJObject((JObject) obj);
            }
            if (obj is JArray)
            {
                return _ParseJArray((JArray) obj);
            }
            if (obj is JValue)
            {
                return _ParseJValue((JValue) obj);
            }

            return obj;
        }

        private static DyObj _ParseJObject(JObject obj)
        {
            var result = new DyObj();

            foreach (var kv in obj as IDictionary<string, JToken>)
            {
                if (kv.Value is JObject)
                {
                    result[kv.Key] = _ParseJObject((JObject) kv.Value);
                }
                else if (kv.Value is JArray)
                {
                    result[kv.Key] = _ParseJArray((JArray) kv.Value);
                }
                else if (kv.Value is JValue)
                {
                    result[kv.Key] = _ParseJValue((JValue) kv.Value);
                }
                else
                {
                    throw new Exception("反序列化为JObject类型时，其实际只会使用到JObject、JArray、JValue三种类型。");
                }
            }

            return result;
        }

        private static IList<object> _ParseJArray(JArray obj)
        {
            var result = new List<object>();

            foreach (var item in obj)
            {
                if (item is JObject)
                {
                    result.Add(_ParseJObject((JObject) item));
                }
                else if (item is JArray)
                {
                    result.Add(_ParseJArray((JArray) item));
                }
                else if (item is JValue)
                {
                    result.Add(_ParseJValue((JValue) item));
                }
                else
                {
                    result.Add(item);
                }
            }

            return result;
        }

        private static object _ParseJValue(JValue obj)
        {
            switch (obj.Type)
            {
                case JTokenType.Float:
                    return Convert.ToDecimal(obj.Value);
                default:
                    return obj.Value;
            }
        }
    }
}