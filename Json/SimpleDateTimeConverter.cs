﻿using System;
using Newtonsoft.Json;
using NiuNai.Bottle.Util;

namespace NiuNai.Bottle.Json
{
    public class SimpleDateTimeConverter : JsonConverter
    {
        internal static bool CanConvert(string str)
        {
            if (string.IsNullOrEmpty(str)) return false;

            DateTime dt;
            if (DateTime.TryParse(str, out dt)) return true;

            return str.StartsWith(@"datetime(", StringComparison.Ordinal) && str.EndsWith(@")", StringComparison.Ordinal);
        }

        internal static DateTime StrToObj(string str)
        {
            DateTime dt;
            if (DateTime.TryParse(str, out dt)) return dt;
            if (!CanConvert(str)) throw new ArgumentException(string.Format("非法的datetime字符串表示 : {0}", str));

            return DateTime.Parse(str.Substring(9, str.Length - 10));
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (!(value is DateTime)) throw new ArgumentException(string.Format("不是合法的DateTime对象 : {0} : {1}", value.GetType(), value));

            var dt = (DateTime)value;
            if (serializer.TypeNameHandling == TypeNameHandling.None)
            {
                writer.WriteValue(dt.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            }
            else
            {
                writer.WriteValue(string.Format(@"datetime({0})", dt.ToString("yyyy-MM-dd HH:mm:ss.fff")));
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.Null:
                case JsonToken.String:
                    break;
                default:
                    throw new ArgumentException(string.Format("无法解析的类型 : {0}.", objectType));
            }

            if (reader.TokenType == JsonToken.Null)
            {
                if (!ObjectUtils.IsNullable(objectType))
                {
                    throw new ArgumentException(string.Format("Cannot convert null value to {0}.", objectType));
                }

                return null;
            }

            if (reader.TokenType == JsonToken.String)
            {
                return StrToObj(reader.Value.ToString());
            }

            throw new ArgumentException(string.Format("Unexpected token parsing date. Expected String, got {0}.", reader.TokenType));
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime) || objectType == typeof(DateTime?);
        }
    }
}