﻿using System;
using Newtonsoft.Json;
using NiuNai.Bottle.Util;

namespace NiuNai.Bottle.Json
{
    public class DecimalConverter : JsonConverter
    {
        internal static bool CanConvert(string str)
        {
            if (string.IsNullOrEmpty(str)) return false;

            return str.StartsWith(@"decimal(", StringComparison.Ordinal) && str.EndsWith(@")", StringComparison.Ordinal);
        }

        internal static decimal StrToObj(string str)
        {
            if (!CanConvert(str)) throw new ArgumentException(string.Format("非法的decimal字符串表示 : {0}", str));

            return decimal.Parse(str.Substring(8, str.Length - 9));
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (!(value is decimal)) throw new ArgumentException(string.Format("不是合法的decimal对象 : {0} : {1}", value.GetType(), value));

            writer.WriteValue(string.Format(@"decimal({0})", value));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.Null:
                case JsonToken.String:
                    break;
                default:
                    throw new ArgumentException(string.Format("无法解析的类型 : {0}.", objectType));
            }

            if (reader.TokenType == JsonToken.Null)
            {
                if (!ObjectUtils.IsNullable(objectType))
                {
                    throw new ArgumentException(string.Format("Cannot convert null value to {0}.", objectType));
                }

                return null;
            }

            if (reader.TokenType == JsonToken.String)
            {
                return StrToObj(reader.Value.ToString());
            }

            throw new ArgumentException(string.Format("Unexpected token parsing decimal. Expected String, got {0}.", reader.TokenType));
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(decimal) || objectType == typeof(decimal?);
        }
    }
}