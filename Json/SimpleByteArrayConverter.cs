﻿using System;
using Newtonsoft.Json;
using NiuNai.Bottle.Lang;

namespace NiuNai.Bottle.Json
{
    public class SimpleByteArrayConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(byte[]);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (null == reader.Value) return null;

            switch (reader.TokenType)
            {
                case JsonToken.Null:
                    return null;
                case JsonToken.String:
                    return reader.Value.ToString().ToBytes();
                default:
                    throw new ArgumentException(string.Format("Unexpected token parsing byte[]. Expected String, got {0}.", reader.TokenType));
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((byte[]) value).ToHex());
        }
    }
}
