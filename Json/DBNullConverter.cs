﻿using System;
using Newtonsoft.Json;
using NiuNai.Bottle.Util;

namespace NiuNai.Bottle.Json
{
    public class DBNullConverter : JsonConverter
    {
        internal static bool CanConvert(string str)
        {
            return !string.IsNullOrEmpty(str) && str.StartsWith(@"DBNull(", StringComparison.Ordinal) &&
                   str.EndsWith(@")", StringComparison.Ordinal);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (!(value is DBNull)) throw new ArgumentException(string.Format("不是合法的DBNull对象 : {0} : {1}", value.GetType(), value));

            writer.WriteValue(@"DBNull()");
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.Null:
                case JsonToken.String:
                    break;
                default:
                    throw new ArgumentException(string.Format("无法解析的类型 : {0}.", objectType));
            }

            if (reader.TokenType == JsonToken.Null)
            {
                if (!ObjectUtils.IsNullable(objectType))
                {
                    throw new ArgumentException(string.Format("Cannot convert null value to {0}.", objectType));
                }

                return DBNull.Value;
            }

            if (reader.TokenType == JsonToken.String)
            {
                var str = reader.Value.ToString();
                if (!CanConvert(str))
                {
                    throw new ArgumentException(string.Format("非法的DBNull字符串表示 : {0}", str));
                }

                return DBNull.Value;
            }

            throw new ArgumentException(string.Format("Unexpected token parsing DBNull. Expected String, got {0}.", reader.TokenType));
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DBNull);
        }
    }
}