﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

namespace NiuNai.Bottle.Lang
{
    /// <summary>
    /// 对与Data相关类型的扩展。
    /// </summary>
    public static class DbDataObjectExtension
    {
        /// <summary>
        /// 将DataTable转换为DyObj列表。
        /// </summary>
        public static List<DyObj> ToDyObjs(this DataTable dt, Func<DataRow, DyObj> rowHandler = null)
        {
            var items = new List<DyObj>();
            if (null == rowHandler)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    items.Add(dr.ToDyObj());
                }
            }
            else
            {
                foreach (DataRow dr in dt.Rows)
                {
                    items.Add(rowHandler(dr));
                }
            }
            return items;
        }

        /// <summary>
        /// 将DataRow转换成DyObj类型，以字段名作为成员属性名。忽略谓词函数返回false的列。
        /// </summary>
        public static DyObj ToDyObj(this DataRow row, Predicate<string> predicate = null)
        {
            if (null == row)
            {
                return null;
            }

            var i = new DyObj();
            if (null == predicate)
            {
                foreach (DataColumn col in row.Table.Columns)
                {
                    i[col.ColumnName] = row[col] is DBNull ? null : row[col];
                }
            }
            else
            {
                foreach (DataColumn col in row.Table.Columns)
                {
                    if (predicate(col.ColumnName))
                    {
                        i[col.ColumnName] = row[col] is DBNull ? null : row[col];
                    }
                }
            }

            return i;
        }

        /// <summary>
        /// 将数据行的参数生成为字符串。
        /// </summary>
        public static string Dump(this DataRow row)
        {
            var sb = new StringBuilder();
            foreach (DataColumn col in row.Table.Columns)
            {
                sb.AppendFormat("{0},", row[col]);
            }
            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }

        /// <summary>
        /// DataReader生成Table。
        /// </summary>
        public static DataTable ToDataTable(this DbDataReader src)
        {
            var dt = new DataTable();
            dt.Load(src);
            return dt;
        }
    }
}