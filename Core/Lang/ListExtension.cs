﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NiuNai.Bottle.Lang
{
    public static class ListExtension
    {
        /// <summary>
        /// 从列表中随机选择元素。
        /// </summary>
        /// <param name="count">选择几个元素</param>
        /// <param name="exclusive">选择的元素是否不可重复</param>
        public static T[] RandomSelect<T>(this IList<T> src, int count = 6, bool exclusive = false)
        {
            if (src.Count <= count)
            {
                throw new ArgumentException("集合的长度必须大于选择的个数。", "count");
            }

            var result = new List<T>();
            if (exclusive)
            {
                var usedIndex = new List<int>();
                for (var i = 0; i < count; ++i)
                {
                    var index = Math.Abs(Guid.NewGuid().GetHashCode()) % src.Count;
                    while (usedIndex.Contains(index))
                    {
                        index = Math.Abs(Guid.NewGuid().GetHashCode()) % src.Count;
                    }
                    usedIndex.Add(index);

                    result.Add(src[index]);
                }
            }
            else
            {
                for (var i = 0; i < count; ++i)
                {
                    result.Add(src[Math.Abs(Guid.NewGuid().GetHashCode()) % src.Count]);
                }
            }

            return result.ToArray();
        }

        /// <summary>
        /// 打乱列表中元素的次序。
        /// </summary>
        public static void Shuffle(this IList src)
        {
            if (null == src || 0 == src.Count) return;

            for (var cursorIndex = src.Count - 1; 0 != cursorIndex; --cursorIndex)
            {
                var tmpCursorIndex = Math.Abs(Guid.NewGuid().GetHashCode()) % cursorIndex;
                var tmpForSwap = src[cursorIndex];
                src[cursorIndex] = src[tmpCursorIndex];
                src[tmpCursorIndex] = tmpForSwap;
            }
        }

        /// <summary>
        /// 用谓词函数移除指定列表元素。
        /// </summary>
        public static void Remove<T>(this IList<T> src, Func<T, bool> predicate)
        {
            var exists = src.Where(predicate).ToArray();
            foreach (var e in exists)
            {
                src.Remove(e);
            }
        }

        /// <summary>
        /// 用谓词函数获取两个序列的投影。
        /// </summary>
        public static IEnumerable<T> Intersact<T>(this IEnumerable<T> src, IEnumerable<T> second, Func<T, T, bool> predicate)
        {
            return src.Where(t => second.Any(a => predicate(t, a)));
        }

        /// <summary>
        /// 用谓词函数联合两个序列。
        /// </summary>
        public static IEnumerable<T> Union<T>(this IEnumerable<T> src, IEnumerable<T> second, Func<T, T, bool> predicate)
        {
            var list = new List<T>(src);

            foreach (var secondeItem in second)
            {
                if (list.Any(a => predicate(a, secondeItem))) continue;

                list.Add(secondeItem);
            }

            return list;
        }

        /// <summary>
        /// 将序列分割成具有指定长度的多个子序列。
        /// </summary>
        public static IList<IList<T>> Split<T>(this IList<T> src, int length)
        {
            if (length <= 0) throw new ArgumentOutOfRangeException("length", length, "指定长度必须大于0");

            var result = new List<IList<T>>();

            var list = new List<T>();
            result.Add(list);
            var sign = length;
            for (var index = 0; index < src.Count; ++index)
            {
                list.Add(src[index]);

                if (index + 1 < sign || sign >= src.Count) continue;

                sign += length;
                list = new List<T>();
                result.Add(list);
            }

            return result;
        }
    }
}