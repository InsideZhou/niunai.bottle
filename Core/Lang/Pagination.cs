﻿using System.Linq;

namespace NiuNai.Bottle.Lang
{
    public class Pagination
    {
        public static int MinPageSize = 1;
        public static int MaxPageSize = 100;

        private Pagination()
        {
        }

        public int SkippedCount
        {
            get
            {
                if (PageIndex <= 0)
                {
                    return 0;
                }

                if (PageSize <= 0)
                {
                    return PageIndex;
                }

                return PageIndex * PageSize;
            }
        }

        public int PageIndex { get; private set; }
        public int PageSize { get; private set; }

        public static Pagination Default(int pageIndex = 1, int pageSize = 5)
        {
            return new Pagination { PageIndex = pageIndex, PageSize = pageSize };
        }

        /// <summary>
        /// 页数。小于0则默认第一页。
        /// </summary>
        public Pagination Index(int index)
        {
            PageIndex = index;
            return this;
        }

        /// <summary>
        /// 每页条目。限制区间为[Pagination.MinPageSize,Pagination.MaxPageSize]。
        /// </summary>
        public Pagination Size(int size)
        {
            PageSize = size > MaxPageSize ? MaxPageSize : (size < MinPageSize ? MinPageSize : size);
            return this;
        }
    }

    public static class PaginationExtension
    {
        /// <summary>
        /// 为有序集合分页并获取指定页。
        /// </summary>
        public static IQueryable<T> Page<T>(this IQueryable<T> query, Pagination pager = null) where T : class
        {
            if (null == pager)
            {
                pager = Pagination.Default();
            }

            return query.Skip(pager.SkippedCount).Take(pager.PageSize);
        }

        /// <summary>
        /// 为有序集合分页并获取指定页。
        /// </summary>
        public static IQueryable<T> Page<T>(this IQueryable<T> query, int pageIndex, int pageSize) where T : class
        {
            var pager = Pagination.Default();
            pager.Index(pageIndex);
            pager.Size(pageSize);

            return query.Skip(pager.SkippedCount).Take(pager.PageSize);
        }

        /// <summary>
        /// 获取有序集合的指定区间，[Pagination.MinPageSize,Pagination.MaxPageSize]。
        /// </summary>
        public static IQueryable<T> Take<T>(this IQueryable<T> query, int offset, int limit) where T : class
        {
            return query.Skip(offset <= 0 ? 0 : offset)
                .Take(limit > Pagination.MaxPageSize ? Pagination.MaxPageSize : (limit < Pagination.MinPageSize ? Pagination.MinPageSize : limit));
        }
    }
}