﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;

namespace NiuNai.Bottle.Lang
{
    public static class MiscExtension
    {
        /// <summary>
        /// 比较字典，并且生成字典间的差异。忽略值是委托类型的属性。被调用者为被比较的旧对象。
        /// </summary>
        /// 差异结果的数据结构描述：
        /// {
        /// before:{} //发生变动的对象属性在变动前的值
        /// , after:{} //发生变动的对象属性在变动后的值
        /// , added:{} //新增的对象属性值
        /// , removed:{} //移除的对象属性值
        /// }
        public static IDictionary<string, object> Diff(this IDictionary<string, object> src,
            IDictionary<string, object> old)
        {
            var before = new DyObj();
            var after = new DyObj();
            var added = new DyObj();
            var removed = new DyObj();

            foreach (var kv in src)
            {
                if (kv.Value is Delegate) continue;

                if (old.ContainsKey(kv.Key))
                {
                    var newValue = kv.Value as IDictionary<string, object>;
                    var oldValue = old[kv.Key] as IDictionary<string, object>;

                    if (null != newValue && null != oldValue)
                    {
                        var subDiff = Diff(newValue, oldValue);
                        if (null != subDiff["after"] || null != subDiff["added"] || null != subDiff["removed"])
                        {
                            before[kv.Key] = oldValue;
                            after[kv.Key] = newValue;
                        }
                    }
                    else if ((null != kv.Value && !kv.Value.Equals(old[kv.Key])) ||
                             (null != old[kv.Key] && !old[kv.Key].Equals(kv.Value)))
                    {
                        before[kv.Key] = old[kv.Key];
                        after[kv.Key] = kv.Value;
                    }
                }
                else
                {
                    added[kv.Key] = kv.Value;
                }
            }

            foreach (var i in old)
            {
                if (!src.ContainsKey(i.Key))
                {
                    removed[i.Key] = i.Value;
                }
            }

            var result = new DyObj();
            result["before"] = ((IDictionary<string, object>)before).Count > 0 ? before : null;
            result["after"] = ((IDictionary<string, object>)after).Count > 0 ? after : null;
            result["added"] = ((IDictionary<string, object>)added).Count > 0 ? added : null;
            result["removed"] = ((IDictionary<string, object>)removed).Count > 0 ? removed : null;

            return result;
        }

        /// <summary>
        /// 获取第一个类型为T的Attribute，若无则返回null。
        /// </summary>
        public static T GetAttribute<T>(this MethodInfo src) where T : Attribute
        {
            return (T)src.GetCustomAttributes(false).FirstOrDefault(attr => attr is T);
        }

        public static DyObj ToDyObj(this NameValueCollection nv)
        {
            var result = new DyObj();
            foreach (var key in nv.AllKeys)
            {
                result[key] = nv[key];
            }

            return result;
        }
    }
}