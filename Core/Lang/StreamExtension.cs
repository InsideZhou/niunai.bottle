﻿using System;
using System.IO;
using System.Text;

namespace NiuNai.Bottle.Lang
{
    public static class StreamExtension
    {
        /// <summary>
        /// 从流生成字节数组。
        /// </summary>
        public static byte[] ToArray(this Stream src)
        {
            if (!src.CanSeek)
            {
                throw new NotSupportedException("不支持无法改变当前Position的流。");
            }

            var currentPosition = src.Position;

            src.Position = 0;
            var result = new byte[src.Length];
            src.Read(result, 0, result.Length);

            src.Position = currentPosition;

            return result;
        }

        /// <summary>
        /// 从流生成字符串。默认编码UTF8。
        /// </summary>
        public static string ToString(this Stream src, Encoding encoding = null)
        {
            using (var reader = new StreamReader(src, encoding ?? Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }
    }
}