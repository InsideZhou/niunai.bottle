﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace NiuNai.Bottle.Lang
{
    public static class StringExtension
    {
        public static List<FontFamily> DefaultFontFamilies { get; set; }

        static StringExtension()
        {
            var fontFamilies = new InstalledFontCollection().Families;
            var families = fontFamilies.Where(fontFamily =>
            {
                switch (fontFamily.Name)
                {
                    case "Arial":
                    case "Comic Sans MS":
                    case "Kristen ITC":
                        return true;
                    default:
                        return false;
                }
            }).ToList();

            if (0 == families.Count)
            {
                families.Add(fontFamilies[0]);
            }

            DefaultFontFamilies = families;
        }

        /// <summary>
        /// 是否Email。
        /// </summary>
        public static bool IsEmail(this string src, string email)
        {
            return Regex.IsMatch(email, @"^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$");
        }

        /// <summary>
        /// 从头部开始移除指定数量的字符。
        /// </summary>
        public static void Remove(this StringBuilder src, int count)
        {
            src.Remove(0, count);
        }

        /// <summary>
        /// 从尾部开始移除指定数量的字符。
        /// </summary>
        public static void RemoveLast(this StringBuilder src, int count)
        {
            src.Remove(src.Length - count, count);
        }

        /// <summary>
        /// 裁剪字符串为省略模式。
        /// </summary>
        /// <param name="length">要裁剪的长度</param>
        /// <param name="placeholder">省略内容的占位符</param>
        public static string Abridge(this string src, int length, string placeholder = "...")
        {
            var str = src.Trim();
            if (str.Length > length)
            {
                str = str.Substring(0, length) + placeholder;
            }

            return str;
        }

        /// <summary>
        /// 用正则表达式移除字符串中所有匹配的字符。默认为空白符。
        /// </summary>
        /// <param name="pattern">匹配的模式</param>
        public static string TrimAbsolute(this string src, string pattern = @"\s")
        {
            if (string.IsNullOrEmpty(src))
            {
                return null;
            }
            return Regex.Replace(src, pattern, string.Empty);
        }

        /// <summary>
        /// 基于正则表达式提取字符串内容，如果正则表达式不匹配，则原样返回；默认的正则表达式可提取被双引号包围的内容。
        /// </summary>
        public static string Substr(this string src, string pattern = @"(?<=^"").+(?=""$)",
            RegexOptions opts = RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.IgnorePatternWhitespace)
        {
            return string.IsNullOrEmpty(src) || !Regex.IsMatch(src, pattern, opts)
                ? src
                : Regex.Match(src, pattern, opts).Value;
        }

        /// <summary>
        /// 分割字符串。
        /// </summary>
        public static string[] Split(this string src, string separator, StringSplitOptions options = StringSplitOptions.None)
        {
            return src.Split(new[] { separator }, options);
        }

        /// <summary>
        /// 左缩进，并用指定字符串填充空间。
        /// </summary>
        /// <param name="padCount">缩进的字符数</param>
        /// <param name="padding">填充物</param>
        public static string PadLeft(this string src, int padCount, string padding = " ")
        {
            if (!string.IsNullOrEmpty(src))
            {
                padCount -= src.Length;
            }

            var sb = new StringBuilder(src ?? string.Empty);
            while (padCount > 0)
            {
                sb.Insert(0, padding);
                padCount--;
            }

            return sb.ToString();
        }

        /// <summary>
        /// 右缩进，并用指定字符串填充空间。
        /// </summary>
        /// <param name="padCount">缩进的字符数</param>
        /// <param name="padding">填充物</param>
        public static string PadRight(this string src, int padCount, string padding = " ")
        {
            if (!string.IsNullOrEmpty(src))
            {
                padCount -= src.Length;
            }

            var sb = new StringBuilder(src ?? string.Empty);
            while (padCount > 0)
            {
                sb.Append(padding);
                padCount--;
            }

            return sb.ToString();
        }

        /// <summary>
        /// 假设字符串是16进制数字的表示，并据此生成字节数组。
        /// </summary>
        /// <returns>如果字符串为空，返回null。</returns>
        public static byte[] ToBytes(this string src)
        {
            if (string.IsNullOrEmpty(src))
            {
                return null;
            }

            try
            {
                if (src.Length <= 2)
                {
                    return new[] { Convert.ToByte(src, 16) };
                }
                var result = new List<byte>();

                var index = src.Length;
                while (index > 2)
                {
                    index -= 2;
                    result.Add(Convert.ToByte(src.Substring(index, 2), 16));
                }
                result.Add(Convert.ToByte(src.Substring(0, index), 16));

                result.Reverse();
                return result.ToArray();
            }
            catch (Exception)
            {
                throw new ArgumentException("非法的十六进制表示形式 : " + src);
            }
        }

        /// <summary>
        /// 将字符串生成为图像。字体随机。
        /// </summary>
        public static Image ToImage(this string src)
        {
            var font = new Font(DefaultFontFamilies.Count == 1 ? DefaultFontFamilies[0] : DefaultFontFamilies.RandomSelect(1)[0], 36);
            return ToImage(src, font);
        }

        /// <summary>
        /// 使用指定字体将字符串生成为图像。背景为白色。
        /// </summary>
        public static Image ToImage(this string src, Font font)
        {
            return ToImage(src, font, Color.White);
        }

        /// <summary>
        /// 使用指定字体、背景颜色将字符串生成为图像。
        /// </summary>
        public static Image ToImage(this string src, Font font, Color bgColor)
        {
            return ToImage(src, font, Color.Black, bgColor);
        }

        /// <summary>
        /// 使用指定字体、背景颜色、字体颜色将字符串生成为图像。
        /// </summary>
        public static Image ToImage(this string src, Font font, Color fontColor, Color bgColor)
        {
            var size = Graphics.FromImage(new Bitmap(1, 1)).MeasureString(src, font);
            var img = new Bitmap(Convert.ToInt32(size.Width), Convert.ToInt32(size.Height));
            var g = Graphics.FromImage(img);

            g.FillRectangle(new SolidBrush(bgColor), 0, 0, img.Width, img.Height);
            g.DrawString(src, font, new SolidBrush(fontColor), 0, 0);

            return img;
        }

        /// <summary>
        /// 生成验证码。
        /// </summary>
        public static Image ToIdentificationImage(this string src)
        {
            var font = new Font("Comic Sans MS", 36);

            var img = ToImage(src, font);
            var result = new Bitmap(img.Width, img.Height);
            var graphic = Graphics.FromImage(result);
            var random = new Random();
            var size = img.Size;

            graphic.FillRectangle(new SolidBrush(Color.White), 0, 0, result.Width, result.Height);

            {
                var points = new List<Point>();
                var gridWidth = img.Size.Width / src.Length / 2;
                for (var grid = 0; grid < src.Length * 2; ++grid)
                {
                    points.Add(new Point(random.Next(gridWidth * grid, gridWidth * (grid + 1)), random.Next(img.Height)));
                }
                graphic.DrawCurve(new Pen(Color.Black, size.Width / src.Length / 6), points.ToArray());
            }

            {
                var brush = new LinearGradientBrush(new Rectangle(0, 0, img.Width, img.Height), Color.CornflowerBlue,
                    Color.Tomato, Convert.ToSingle(random.NextDouble() * 180));
                brush.InterpolationColors = _GenerateBlend(src.Length);
                graphic.DrawString(src, font, brush, 0, 0);
            }

            {
                var paddingW = Convert.ToInt32(size.Width / src.Length / 3);
                var paddingH = Convert.ToInt32(size.Height) / 6;
                var points = new List<Point>();
                var count = random.Next(1, src.Length);
                points.Add(new Point(paddingW, random.Next(paddingH, img.Height)));
                while (count-- >= 0)
                {
                    points.Add(new Point(random.Next(paddingW, img.Width), random.Next(paddingH, img.Height)));
                }
                points.Add(new Point(img.Width, random.Next(paddingH, img.Height)));
                graphic.DrawCurve(new Pen(Color.Black, size.Width / src.Length / 6), points.ToArray());
            }

            return result;
        }

        private static ColorBlend _GenerateBlend(int colorNum = 3)
        {
            var random = new Random();
            var blend = new ColorBlend();
            var colors = (from KnownColor item in Enum.GetValues(typeof(KnownColor)) let c = Color.FromKnownColor(item) select c).ToList().RandomSelect(colorNum).ToList();
            colors.Shuffle();
            blend.Colors = colors.ToArray();

            var positions = new List<float> { 0f };
            while (positions.Count < colors.Count - 1)
            {
                positions.Add(Convert.ToSingle(random.NextDouble()));
            }
            positions.Add(1.0f);
            blend.Positions = positions.ToArray();

            return blend;
        }
    }
}