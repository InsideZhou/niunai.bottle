﻿using System;
using System.Data;
using System.Data.Common;
using System.Text;

namespace NiuNai.Bottle.Lang
{
    /// <summary>
    /// 对DbCommand的扩展。与ado.net同名但附带Auto后缀的方法表明，此方法可自动管理数据库连接的打开、关闭、事务。
    /// </summary>
    /// <remarks>
    /// 为提供ExecuteReader的同名Auto方法是由于内置自动打开和关闭数据库连接，而连接关闭后DbDataReader就失效了，所以无法当作方法结果传出。
    /// </remarks>
    public static class DbCommandExtension
    {
        public static DbCommand AddParameter(this DbCommand comm, string name, object value, ParameterDirection direction = ParameterDirection.Input, DbType? type = null)
        {
            var p = comm.CreateParameter();
            p.ParameterName = name;
            try
            {
                p.Value = value;
            }
            catch (ArgumentException e)
            {
                throw new ArgumentException(string.Format("{0}\nName : {1};Value : {2}", e.Message, name, value), e);
            }
            p.Direction = direction;
            if (null != type)
            {
                p.DbType = (DbType) type;
            }
            comm.Parameters.Add(p);
            return comm;
        }

        /// <summary>
        /// 用于添加位置参数。例如："select * from table where oid = ?"
        /// </summary>
        public static DbCommand AddParameters(this DbCommand comm, params object[] values)
        {
            for (int i = 0; i < values.Length; ++i)
            {
                var v = values[i];
                if (v is DbParameter)
                {
                    comm.Parameters.Add(v);
                }
                else
                {
                    var p = comm.CreateParameter();
                    try
                    {
                        p.Value = v;
                    }
                    catch (ArgumentException e)
                    {
                        throw new ArgumentException(string.Format("{0}\nIndex : {1};Value : {2}", e.Message, i, v), e);
                    }
                    comm.Parameters.Add(p);
                }
            }

            return comm;
        }

        /// <summary>
        /// 添加参数至命令。
        /// </summary>
        public static DbCommand AddParameters(this DbCommand comm, params DbParameter[] ps)
        {
            comm.Parameters.AddRange(ps);
            return comm;
        }

        /// <summary>
        /// 输出此命令的文本和参数。
        /// </summary>
        public static string Dump(this DbCommand comm)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(comm.CommandText);

            foreach (DbParameter p in comm.Parameters)
            {
                sb.AppendFormat("{0} : {1}\n", string.IsNullOrEmpty(p.ParameterName) ? comm.Parameters.IndexOf(p).ToString() : p.ParameterName, p.Value);
            }

            return sb.ToString();
        }

        /// <summary>
        /// 获取数据表。
        /// </summary>
        public static DataTable GetDataTable(this DbCommand comm)
        {
            using (var reader = comm.ExecuteReader(CommandBehavior.SingleResult))
            {
                return reader.ToDataTable();
            }
        }
    }
}