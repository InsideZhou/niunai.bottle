﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using NiuNai.Bottle.Util;

namespace NiuNai.Bottle.Lang
{
    /// <summary>
    ///     属性名大小写不敏感的动态类型。
    /// </summary>
    /// <remarks>
    ///     继承IDictionary&lt;string, object&gt;是为了在Json序列化时被识别为一个字典。
    /// </remarks>
    public class DyObj : DynamicObject, ICloneable, IDictionary<string, object>
    {
        private readonly IDictionary<string, object> _dict = new Dictionary<string, object>();

        public DyObj()
        {
        }

        public DyObj(IDictionary<string, object> dict)
        {
            foreach (var kv in dict)
            {
                _dict[kv.Key.ToLower()] = kv.Value;
            }
        }

        public List<string> Attrs
        {
            get { return _dict.Keys.ToList(); }
        }

        /// <summary>
        ///     要获取的键不存在时返回null。
        ///     键的大小写不敏感。
        /// </summary>
        public object this[string attr]
        {
            get
            {
                var k = attr.ToLower();
                return _dict.ContainsKey(k) ? _dict[k] : null;
            }
            set { _dict[attr.ToLower()] = value; }
        }

        #region ICloneable 成员

        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion

        #region IEnumerable<KeyValuePair<string, object>> 成员

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return _dict.GetEnumerator();
        }

        #endregion

        #region IEnumerable 成员

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        public bool ContainsAttr(string attr)
        {
            return _dict.ContainsKey(attr.ToLower());
        }

        /// <summary>
        ///     深度克隆此对象。
        /// </summary>
        /// <remarks>
        ///     属性值的类型必须为string、基元类型、值类型、ICloneable之一。
        ///     如果属性值为引用类型且实现了ICloneable，务必保证其实现为深度克隆，否则无法保证深度克隆行为的准确性。
        /// </remarks>
        public DyObj Clone()
        {
            var dy = new DyObj();
            foreach (var kv in _dict)
            {
                if (ObjectUtils.IsNil(kv.Value))
                {
                    dy._dict[kv.Key] = kv.Value;
                    continue;
                }

                var t = kv.Value.GetType();
                if (kv.Value is string || t.IsPrimitive || t.IsValueType)
                {
                    dy._dict[kv.Key] = kv.Value;
                }
                else if (kv.Value is ICloneable)
                {
                    dy._dict[kv.Key] = (kv.Value as ICloneable).Clone();
                }
                else
                {
                    throw new Exception(string.Format("{0} : {1}，该键的值无法被克隆。", kv.Key, kv.Value));
                }
            }

            return dy;
        }

        public DyObj Diff(DyObj dy)
        {
            var diff = _dict.Diff(dy._dict);
            return diff is DyObj ? (DyObj) diff : new DyObj(diff);
        }

        /// <summary>
        ///     过滤对象的属性值，并用过滤后留下的属性及其值生成新的对象。
        /// </summary>
        /// <param name="predicate">谓词函数，接受属性名，返回true时此属性保留用于生成新对象。</param>
        /// <returns>如果任何属性都不符合谓词函数的要求，返回null。</returns>
        public DyObj Filter(Func<string, bool> predicate)
        {
            var result = new DyObj();

            foreach (var item in _dict)
            {
                if (predicate(item.Key))
                {
                    result[item.Key] = item.Value;
                }
            }

            return result._dict.Count > 0 ? result : null;
        }

        /// <summary>
        ///     将对象所有属性的值设为null。
        /// </summary>
        public void Reset()
        {
            foreach (var attr in Attrs)
            {
                _dict[attr] = null;
            }
        }

        /// <summary>
        ///     将对象所有属性清空。
        /// </summary>
        public void Clear()
        {
            _dict.Clear();
        }

        /// <summary>
        ///     指示本对象在逻辑上是否只包含空值。
        /// </summary>
        public bool IsPlainEmpty()
        {
            foreach (var v in _dict.Values)
            {
                if (!ObjectUtils.IsNil(v))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        ///     非递归合并两个DyObj的属性。
        /// </summary>
        /// <param name="dy">被合并的目标</param>
        public void Merge(DyObj dy, DyObjMergeType mergeType = DyObjMergeType.Combine)
        {
            if (null == dy)
            {
                throw new ArgumentNullException("dy");
            }

            switch (mergeType)
            {
                case DyObjMergeType.Combine:
                    foreach (var kv in dy)
                    {
                        if (!_dict.ContainsKey(kv.Key))
                        {
                            _dict[kv.Key] = kv.Value;
                        }
                    }
                    break;
                case DyObjMergeType.CombineAndOverwrite:
                    foreach (var kv in dy)
                    {
                        _dict[kv.Key] = kv.Value;
                    }
                    break;
                case DyObjMergeType.Overwrite:
                    foreach (var key in (from kv in _dict select kv.Key).ToList())
                    {
                        if (dy._dict.ContainsKey(key))
                        {
                            _dict[key] = dy._dict[key];
                        }
                    }
                    break;
                default:
                    throw new Exception("代码不该到达这个位置。");
            }
        }

        /// <summary>
        ///     删除对象属性。
        /// </summary>
        public bool Remove(string attr)
        {
            return _dict.Remove(attr.ToLower());
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            return _dict.TryGetValue(binder.Name.ToLower(), out result);
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            _dict[binder.Name.ToLower()] = value;
            return true;
        }

        #region IDictionary<string,object> 成员

        void IDictionary<string, object>.Add(string key, object value)
        {
            _dict.Add(key.ToLower(), value);
        }

        bool IDictionary<string, object>.ContainsKey(string key)
        {
            return ContainsAttr(key);
        }

        ICollection<string> IDictionary<string, object>.Keys
        {
            get { return Attrs; }
        }

        bool IDictionary<string, object>.Remove(string key)
        {
            return Remove(key);
        }

        bool IDictionary<string, object>.TryGetValue(string key, out object value)
        {
            return _dict.TryGetValue(key, out value);
        }

        ICollection<object> IDictionary<string, object>.Values
        {
            get { return _dict.Values; }
        }

        object IDictionary<string, object>.this[string key]
        {
            get { return this[key]; }
            set { this[key] = value; }
        }

        #endregion

        #region ICollection<KeyValuePair<string,object>> 成员

        void ICollection<KeyValuePair<string, object>>.Add(KeyValuePair<string, object> item)
        {
            _dict.Add(item);
        }

        void ICollection<KeyValuePair<string, object>>.Clear()
        {
            _dict.Clear();
        }

        bool ICollection<KeyValuePair<string, object>>.Contains(KeyValuePair<string, object> item)
        {
            return _dict.Contains(item);
        }

        void ICollection<KeyValuePair<string, object>>.CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
        {
            _dict.CopyTo(array, arrayIndex);
        }

        int ICollection<KeyValuePair<string, object>>.Count
        {
            get { return _dict.Count; }
        }

        bool ICollection<KeyValuePair<string, object>>.IsReadOnly
        {
            get { return _dict.IsReadOnly; }
        }

        bool ICollection<KeyValuePair<string, object>>.Remove(KeyValuePair<string, object> item)
        {
            return _dict.Remove(item);
        }

        #endregion
    }

    public enum DyObjMergeType
    {
        /// <summary>
        ///     只合并目标对象有而自身没有的属性。
        /// </summary>
        Combine,

        /// <summary>
        ///     将目标对象所有属性合并至自身，自身的同名属性值被覆盖。
        /// </summary>
        CombineAndOverwrite,

        /// <summary>
        ///     只用目标对象的属性值覆盖自身的同名属性值。
        /// </summary>
        Overwrite
    }
}