﻿using System.Text;

namespace NiuNai.Bottle.Lang
{
    public static class ByteExtension
    {
        /// <summary>
        /// 从字节数组生成其相应的字符串表示。
        /// </summary>
        /// <param name="formatStr">生成字符串时采用的格式。默认为"X2"。</param>
        public static string ToHex(this byte[] src, string formatStr = "X2")
        {
            var sb = new StringBuilder();
            foreach (var b in src)
            {
                sb.Append(b.ToString(formatStr));
            }
            return sb.ToString();
        }
    }
}