﻿namespace NiuNai.Bottle.Log
{
    /// <summary>
    /// 默认记录到ROOT Logger。
    /// </summary>
    public interface ILogger
    {
        void Trace(string msg);
        void Trace(string logger, string msg);

        void Debug(string msg);
        void Debug(string logger, string msg);

        void Info(string msg);
        void Info(string logger, string msg);

        void Warn(string msg);
        void Warn(string logger, string msg);

        void Error(string msg);
        void Error(string logger, string msg);
    }
}
