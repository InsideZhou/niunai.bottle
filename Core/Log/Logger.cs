﻿namespace NiuNai.Bottle.Log
{
    public class Logger
    {
        private static ILogger _logger = new NullLogger();

        public static ILogger Instance
        {
            get { return _logger; }
            set { _logger = value; }
        }

        Logger()
        {
        }
    }
}
