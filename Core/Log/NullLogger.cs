﻿namespace NiuNai.Bottle.Log
{
    public class NullLogger : ILogger
    {
        public void Trace(string msg)
        {
        }

        public void Trace(string logger, string msg)
        {
        }

        public void Debug(string msg)
        {
        }

        public void Debug(string logger, string msg)
        {
        }

        public void Info(string msg)
        {
        }

        public void Info(string logger, string msg)
        {
        }

        public void Warn(string msg)
        {
        }

        public void Warn(string logger, string msg)
        {
        }

        public void Error(string msg)
        {
        }

        public void Error(string logger, string msg)
        {
        }
    }
}
