﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using NiuNai.Bottle.Lang;

namespace NiuNai.Bottle.Util
{
    public class ObjectUtils
    {
        public static bool IsNullable(object src)
        {
            if (null == src) return true;

            var t = src is Type ? (Type) src : src.GetType();

            if (t.IsValueType)
            {
                return t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>);
            }

            return true;
        }

        /// <summary>
        /// 综合判断对象是否为null或者值的含义为空（例如DBNull.Value、string.Empty）。
        /// </summary>
        public static bool IsNil(object src)
        {
            if (!IsNullable(src)) return false;
            if (src is DBNull) return true;
            if (src is string) return string.IsNullOrEmpty(src as string);

            return null == src;
        }

        /// <summary>
        /// 生成限定长度的随机字符串。
        /// </summary>
        public static string GenerateRandomStr(string baseStr, int count = 6)
        {
            return GenerateRandomStr(count, baseStr);
        }

        /// <summary>
        /// 用指定字符串生成限定长度的随机字符串。
        /// </summary>
        public static string GenerateRandomStr(int count = 6, string baseStr = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        {
            return new string(baseStr.ToArray().RandomSelect(count));
        }

        /// <summary>
        /// 用指定字符串生成随机长度的随机字符串。
        /// </summary>
        public static string GenerateRandomStr(int minLen, int maxLen, string baseStr = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        {
            return new string(baseStr.ToArray().RandomSelect(new Random().Next(minLen, maxLen + 1)));
        }

        /// <summary>
        /// 获取枚举类型的字符串表示与其对应值，值不是枚举类型。
        /// </summary>
        public static List<Tuple<string, object>> GetEnumKVs(Type enumType)
        {
            var result = new List<Tuple<string, object>>();
            foreach (var val in Enum.GetValues(enumType))
            {
                result.Add(new Tuple<string, object>(val.ToString(), Convert.ChangeType(val, Enum.GetUnderlyingType(enumType))));
            }

            return result;
        }

        /// <summary>
        /// 将匿名类型转为字典。
        /// </summary>
        public static IDictionary<string, object> AnonymousObjectToDict(object anony)
        {
            var result = new DyObj();
            foreach (PropertyDescriptor item in TypeDescriptor.GetProperties(anony))
            {
                result[item.Name] = item.GetValue(anony);
            }

            return result;
        }
    }
}