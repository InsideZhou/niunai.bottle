﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using SystemIsolationLevel = System.Transactions.IsolationLevel;
using SystemTransaction = System.Transactions.Transaction;
using SystemTransactionScope = System.Transactions.TransactionScope;

namespace NiuNai.Bottle.Util
{
    /// <summary>
    /// 为了简化、方便执行参数化查询的帮助类，支持事务。
    /// <para>1、依赖配置文件（web.config或app.confg）中的DbProviderFactories节，除.net的machine.config默认自带之外，使用到的Provider必须首先在此注册其工厂。</para>
    /// <para>
    /// 2、配置文件中connectionStrings节必须首先使用clear来清除继承自machine.config中的设置，否则 DefaultConnStr 和 DefaultProviderName
    /// 将初始化为machine.config中第一项。
    /// </para>
    /// <para>3、实例不支持并发，在并发环境下，每个线程应使用不同的实例。</para>
    /// </summary>
    public class SqlUtils : IDisposable
    {
        public static int DefaultCommandTimeout = 15;
        private static ConnectionStringSettings _defaultConnSettings;

        /// <summary>
        /// 使用默认设置初始化。
        /// </summary>
        public SqlUtils()
            : this(DefaultConnSettings)
        {
        }

        /// <summary>
        /// 用指定的连接字符串和数据提供程序获取SQLHelper实例。
        /// </summary>
        /// <param name="connSettings"></param>
        public SqlUtils(ConnectionStringSettings connSettings)
        {
            if (null == connSettings)
            {
                throw new ArgumentNullException();
            }

            try
            {
                Factory = DbProviderFactories.GetFactory(connSettings.ProviderName);
            }
            catch (ConfigurationErrorsException e)
            {
                throw new ConfigurationErrorsException(string.Format("{0} : {1}", e.Message, connSettings.ProviderName));
            }

            Conn = Factory.CreateConnection();
            Conn.ConnectionString = connSettings.ConnectionString;
        }

        /// <summary>
        /// 当前实例使用的连接。
        /// </summary>
        public DbConnection Conn { get; private set; }

        /// <summary>
        /// 当前实例出自的Factory。
        /// </summary>
        public DbProviderFactory Factory { get; private set; }

        /// <summary>
        /// 默认初始化为应用程序配置文件中connectionStrings节第一项。
        /// </summary>
        public static ConnectionStringSettings DefaultConnSettings
        {
            get
            {
                if (null == _defaultConnSettings && ConfigurationManager.ConnectionStrings.Count > 0)
                {
                    _defaultConnSettings = ConfigurationManager.ConnectionStrings[0];
                }

                return _defaultConnSettings;
            }
            set { _defaultConnSettings = value; }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~SqlUtils()
        {
            Dispose(false);
        }

        /// <summary>
        /// 创建参数。
        /// </summary>
        public DbParameter CreateParameter()
        {
            return Factory.CreateParameter();
        }

        /// <summary>
        /// 初始化命令。
        /// </summary>
        public DbCommand Init(string commText)
        {
            return Init(commText, CommandType.Text, DefaultCommandTimeout);
        }

        /// <summary>
        /// 初始化命令。可指定命令超时时间，时间单位为命令默认。
        /// </summary>
        public DbCommand Init(string commText, int commTimeout)
        {
            return Init(commText, CommandType.Text, commTimeout);
        }

        /// <summary>
        /// 初始化指定类型的命令。
        /// </summary>
        public DbCommand Init(string commText, CommandType commType)
        {
            return Init(commText, commType, DefaultCommandTimeout);
        }

        /// <summary>
        /// 初始化命令。
        /// </summary>
        public DbCommand Init(string commText, CommandType commType, int commTimeout)
        {
            var comm = Conn.CreateCommand();
            comm.CommandText = commText;
            comm.CommandType = commType;
            comm.CommandTimeout = commTimeout;
            return comm;
        }

        /// <summary>
        /// 初始化数据适配器。
        /// </summary>
        public DbDataAdapter InitAdapter(string selectTxt)
        {
            var adapter = Factory.CreateDataAdapter();
            adapter.SelectCommand = Init(selectTxt);

            var commBuilder = Factory.CreateCommandBuilder();
            commBuilder.DataAdapter = adapter;

            return adapter;
        }

        protected virtual void Dispose(bool includeManagedRes)
        {
            if (includeManagedRes)
            {
                Factory = null;
            }
            Conn.Dispose();
        }

        public static IsolationLevel ScopeTransactionIsolationToDbTransactionIsolation(SystemIsolationLevel isolation)
        {
            switch (isolation)
            {
                case SystemIsolationLevel.Chaos:
                    return IsolationLevel.Chaos;
                case SystemIsolationLevel.ReadCommitted:
                    return IsolationLevel.ReadCommitted;
                case SystemIsolationLevel.ReadUncommitted:
                    return IsolationLevel.ReadUncommitted;
                case SystemIsolationLevel.RepeatableRead:
                    return IsolationLevel.RepeatableRead;
                case SystemIsolationLevel.Serializable:
                    return IsolationLevel.Serializable;
                case SystemIsolationLevel.Snapshot:
                    return IsolationLevel.Snapshot;
                default:
                    return IsolationLevel.Unspecified;
            }
        }

        public static SystemIsolationLevel DbTransactionIsolationToScopeTransactionIsolation(IsolationLevel isolation)
        {
            switch (isolation)
            {
                case IsolationLevel.Chaos:
                    return SystemIsolationLevel.Chaos;
                case IsolationLevel.ReadCommitted:
                    return SystemIsolationLevel.ReadCommitted;
                case IsolationLevel.ReadUncommitted:
                    return SystemIsolationLevel.ReadUncommitted;
                case IsolationLevel.RepeatableRead:
                    return SystemIsolationLevel.RepeatableRead;
                case IsolationLevel.Serializable:
                    return SystemIsolationLevel.Serializable;
                case IsolationLevel.Snapshot:
                    return SystemIsolationLevel.Snapshot;
                default:
                    return SystemIsolationLevel.Unspecified;
            }
        }
    }
}