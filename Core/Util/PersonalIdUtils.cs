﻿using System;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace NiuNai.Bottle.Util
{
    /// <summary>
    /// 处理中华人民共和国居民身份证号码。
    /// ConvertT18与Convert15To18的区别是，如果前者输入一个18位身份证，则原样返回，后者会抛出FormatException。
    /// ConvertT15与Convert18To15的区别也类似。
    /// </summary>
    public class PersonalIdUtils
    {
        public static bool Validate(string id)
        {
            if (string.IsNullOrEmpty(id)) return false;

            try
            {
                DateTime dt;
                switch (id.Length)
                {
                    case 15:
                        return DateTime.TryParseExact(id.Substring(6, 6), "yyMMdd", CultureInfo.CurrentCulture,
                            DateTimeStyles.None, out dt)
                               && Regex.IsMatch(id, @"^\d{15}$");
                    case 18:
                        return DateTime.TryParseExact(id.Substring(6, 8), "yyyyMMdd", CultureInfo.CurrentCulture,
                            DateTimeStyles.None, out dt)
                               && Regex.IsMatch(id, @"^\d{18}$|^\d{17}X$", RegexOptions.IgnoreCase)
                               && id[17] == _CalcCheckCode(id);
                    default:
                        return false;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool IsMale(string id)
        {
            if (!Validate(id)) throw new FormatException("不是有效的身份证格式");

            switch (id.Length)
            {
                case 15:
                    return Convert.ToBoolean(Convert.ToInt32(id[14]) & 1);
                default:
                    return Convert.ToBoolean(Convert.ToInt32(id[16]) & 1);
            }
        }

        public static string ConvertTo18(string id)
        {
            if (!Validate(id)) throw new FormatException("不是有效的身份证格式");
            return 18 == id.Length ? id : Convert15To18(id);
        }

        public static string ConvertTo15(string id)
        {
            if (!Validate(id)) throw new FormatException("不是有效的身份证格式");
            return 15 == id.Length ? id : Convert18To15(id);
        }

        public static string Convert15To18(string id)
        {
            if (!Validate(id)) return id;

            var result = new StringBuilder(id);
            result.Insert(6, "19");
            result.Insert(result.Length, _CalcCheckCode(result.ToString()).ToString());
            return result.ToString();
        }

        public static string Convert18To15(string id)
        {
            if (!Validate(id)) return id;

            var result = new StringBuilder(id);
            result.Remove(6, 2);
            result.Remove(result.Length - 1, 1);
            return result.ToString();
        }

        private static char _CalcCheckCode(string id)
        {
            //系数的算法是(2^(18-从1开始算起的序号)) mod 11
            var factors = new[] { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };
            var checkIndex = 0;
            for (var index = 0; index < 17; ++index)
            {
                checkIndex += int.Parse(id[index].ToString()) * factors[index];
            }
            checkIndex %= 11;

            return new[] { '1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2' }[checkIndex];
        }
    }
}