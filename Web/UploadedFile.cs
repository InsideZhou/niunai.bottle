﻿using System.IO;
using System.Web;

namespace NiuNai.Bottle.Web
{
    public class UploadedFile : HttpPostedFileBase
    {
        private readonly int _contentLength;
        private readonly string _contentType;
        private readonly string _fileName;
        private readonly Stream _stream;

        public UploadedFile(HttpPostedFile file) : this(file.FileName, file.ContentType, file.InputStream)
        {
        }

        public UploadedFile(HttpPostedFileBase file) : this(file.FileName, file.ContentType, file.InputStream)
        {
        }

        public UploadedFile(string filename, string contentType, Stream stream)
        {
            _fileName = filename;
            _contentType = contentType;
            _contentLength = (int) stream.Length;
            _stream = stream;
        }

        public override int ContentLength
        {
            get { return _contentLength; }
        }

        public override string FileName
        {
            get { return _fileName; }
        }

        public override string ContentType
        {
            get { return _contentType; }
        }

        public override Stream InputStream
        {
            get { return _stream; }
        }

        public override void SaveAs(string filename)
        {
            using (var file = File.OpenWrite(Path.GetFullPath(filename)))
            {
                InputStream.CopyTo(file);
            }
        }
    }
}