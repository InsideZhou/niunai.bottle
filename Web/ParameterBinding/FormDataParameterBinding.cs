﻿using System.Collections.Specialized;
using System.ComponentModel;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Metadata;
using NiuNai.Bottle.Json;

namespace NiuNai.Bottle.Web.ParameterBinding
{
    public class FormDataParameterBinding : AbstractParameterBinding
    {
        public const string Key_FormDataProvider = "NiuNai.Bottle.Web.ParameterBinding:formdataProvider";

        public FormDataParameterBinding(HttpParameterDescriptor descriptor)
            : base(descriptor)
        {
        }

        public override bool WillReadBody
        {
            get { return true; }
        }

        public override async Task ExecuteBindingAsync(ModelMetadataProvider metadataProvider, HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            if (!actionContext.Request.Content.IsFormData())
            {
                throw new ParameterBindingException("Mime必须是application/x-www-form-urlencoded。");
            }

            var formdataProvider = actionContext.Request.Properties.ContainsKey(Key_FormDataProvider) ? (NameValueCollection) actionContext.Request.Properties[Key_FormDataProvider] : null;
            if (null == formdataProvider)
            {
                formdataProvider = new NameValueCollection();
                actionContext.Request.Properties[Key_FormDataProvider] = formdataProvider;

                formdataProvider.Add(await actionContext.Request.Content.ReadAsFormDataAsync(cancellationToken));
            }

            var parameterName = Descriptor.Prefix + Descriptor.ParameterName;
            var result = formdataProvider[parameterName];

            if (null == result)
            {
                SetValue(actionContext, Descriptor.DefaultValue);
                return;
            }

            if (Descriptor.ParameterType == typeof(string))
            {
                SetValue(actionContext, result);
                return;
            }

            if (!Descriptor.ParameterType.IsValueType && Converter == Converter.Json)
            {
                SetValue(actionContext, JsonUtils.Deserialize(Descriptor.ParameterType, result));
                return;
            }

            var converter = TypeDescriptor.GetConverter(Descriptor.ParameterType);
            if (converter.CanConvertFrom(typeof(string)))
            {
                SetValue(actionContext, converter.ConvertFromString(result));
            }

            SetValue(actionContext, Descriptor.DefaultValue);
        }
    }

    public class FormDataAttribute : AbstractParameterBindingAttribute
    {
        public FormDataAttribute()
            : this(Converter.Json)
        {
        }

        /// <param name="converter">待绑定的参数需要的转换</param>
        public FormDataAttribute(Converter converter)
            : base(converter)
        {
        }

        public override HttpParameterBinding GetBinding(HttpParameterDescriptor descriptor)
        {
            return new FormDataParameterBinding(descriptor) { Converter = Converter };
        }
    }
}