﻿using System;
using System.Linq;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;

namespace NiuNai.Bottle.Web.ParameterBinding
{
    public class NiuNaiActionValueBinder : DefaultActionValueBinder
    {
        public override HttpActionBinding GetBinding(HttpActionDescriptor actionDescriptor)
        {
            if (actionDescriptor == null) throw new ArgumentNullException("actionDescriptor");

            var bindings = Array.ConvertAll(actionDescriptor.GetParameters().ToArray(), GetParameterBinding);
            var actionBinding = new HttpActionBinding(actionDescriptor, bindings);
            EnsureOneBodyParameter(actionBinding);
            return actionBinding;
        }

        private void EnsureOneBodyParameter(HttpActionBinding actionBinding)
        {
            var groups = actionBinding.ParameterBindings.Where(item => item.WillReadBody).GroupBy(item => item.Descriptor.ParameterBinderAttribute.GetType()).ToList();

            if (groups.All(item => typeof(MultipartParameterBinding).IsAssignableFrom(item.Key)) || groups.All(item => typeof(FormDataParameterBinding).IsAssignableFrom(item.Key)))
            {
                return;
            }

            if (groups.Count > 1)
            {
                var parameterBinding = actionBinding.ParameterBindings.First(item => item.WillReadBody);
                var index = actionBinding.ParameterBindings.ToList().IndexOf(parameterBinding);
                var message = string.Format("不能同时绑定多个用不同方式读取RequestBody的参数{0}。", parameterBinding.Descriptor.ParameterName);
                actionBinding.ParameterBindings[index] = new ErrorParameterBinding(parameterBinding.Descriptor, message);
            }
        }
    }
}