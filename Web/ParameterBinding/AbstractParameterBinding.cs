﻿using System.Web.Http;
using System.Web.Http.Controllers;

namespace NiuNai.Bottle.Web.ParameterBinding
{
    public abstract class AbstractParameterBinding : HttpParameterBinding
    {
        protected AbstractParameterBinding(HttpParameterDescriptor descriptor)
            : base(descriptor)
        {
        }

        /// <summary>
        /// 待绑定的参数需要的转换。
        /// </summary>
        public Converter Converter { get; set; }
    }

    public abstract class AbstractParameterBindingAttribute : ParameterBindingAttribute
    {
        protected AbstractParameterBindingAttribute(Converter converter)
        {
            Converter = converter;
        }

        /// <summary>
        /// 指明该输入绑定至参数前需要的转换。
        /// </summary>
        public Converter Converter { get; set; }
    }

    /// <summary>
    /// 转换器的指示。
    /// </summary>
    public enum Converter
    {
        Raw, Json
    }
}