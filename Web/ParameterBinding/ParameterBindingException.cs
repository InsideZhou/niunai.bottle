﻿using System;

namespace NiuNai.Bottle.Web.ParameterBinding
{
    public class ParameterBindingException : Exception
    {
        private readonly string _msg = "";

        public ParameterBindingException()
        {
        }

        public ParameterBindingException(string errorMsg)
        {
            _msg = errorMsg;
        }

        public override string Message
        {
            get { return _msg; }
        }
    }
}
