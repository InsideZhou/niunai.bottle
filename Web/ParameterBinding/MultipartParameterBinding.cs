﻿using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Metadata;
using NiuNai.Bottle.Json;

namespace NiuNai.Bottle.Web.ParameterBinding
{
    public class MultipartParameterBinding : AbstractParameterBinding
    {
        public const string Key_MultipartProvider = "NiuNai.Bottle.Web.ParameterBinding:multipartProvider";

        public MultipartParameterBinding(HttpParameterDescriptor descriptor)
            : base(descriptor)
        {
        }

        public override bool WillReadBody
        {
            get { return true; }
        }

        public override async Task ExecuteBindingAsync(ModelMetadataProvider metadataProvider, HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            if (!actionContext.Request.Content.IsMimeMultipartContent("form-data"))
            {
                throw new ParameterBindingException("Mime必须是multipart/form-data。");
            }

            var multipartProvider = actionContext.Request.Properties.ContainsKey(Key_MultipartProvider) ? (MultipartStreamProvider) actionContext.Request.Properties[Key_MultipartProvider] : null;
            if (null == multipartProvider)
            {
                multipartProvider = new MultipartMemoryStreamProvider();
                actionContext.Request.Properties[Key_MultipartProvider] = multipartProvider;

                await actionContext.Request.Content.ReadAsMultipartAsync(multipartProvider, cancellationToken);
            }

            var parameterName = Descriptor.Prefix + Descriptor.ParameterName;
            var content = multipartProvider.Contents.FirstOrDefault(item => item.Headers.ContentDisposition.Name.Trim('"') == parameterName);

            if (null == content)
            {
                if (!Descriptor.IsOptional) throw new ParameterBindingException(string.Format("没有找到该参数{0}。", Descriptor.ParameterName));
                SetValue(actionContext, null);
                return;
            }

            if (string.IsNullOrEmpty(content.Headers.ContentDisposition.FileName))
            {
                await content.ReadAsStringAsync().ContinueWith(task =>
                {
                    if (null == task)
                    {
                        SetValue(actionContext, Descriptor.DefaultValue);
                        return;
                    }

                    if (Descriptor.ParameterType == typeof(string))
                    {
                        SetValue(actionContext, task.Result);
                        return;
                    }

                    if (!Descriptor.ParameterType.IsValueType && Converter == Converter.Json)
                    {
                        SetValue(actionContext, JsonUtils.Deserialize(Descriptor.ParameterType, task.Result));
                        return;
                    }

                    var converter = TypeDescriptor.GetConverter(Descriptor.ParameterType);
                    if (!converter.CanConvertFrom(typeof(string)))
                    {
                        SetValue(actionContext, Descriptor.DefaultValue);
                        return;
                    }

                    SetValue(actionContext, converter.ConvertFromString(task.Result));
                }, TaskContinuationOptions.OnlyOnRanToCompletion);

                return;
            }

            if (typeof(HttpPostedFileBase) != Descriptor.ParameterType && !typeof(UploadedFile).IsAssignableFrom(Descriptor.ParameterType))
            {
                throw new ParameterBindingException("上传文件只能解析为System.Web.HttpPostedFileBase及其子类型。");
            }

            var fileName = content.Headers.ContentDisposition.FileName.Trim('"');
            var contentType = content.Headers.ContentType.MediaType;
            await content.ReadAsStreamAsync().ContinueWith(task =>
            {
                SetValue(actionContext, new UploadedFile(fileName, contentType, task.Result));
            }, TaskContinuationOptions.OnlyOnRanToCompletion);
        }
    }

    public class MultipartAttribute : AbstractParameterBindingAttribute
    {
        public MultipartAttribute()
            : this(Converter.Json)
        {
        }

        /// <param name="converter">待绑定的参数需要的转换</param>
        public MultipartAttribute(Converter converter)
            : base(converter)
        {
        }

        public override HttpParameterBinding GetBinding(HttpParameterDescriptor descriptor)
        {
            return new MultipartParameterBinding(descriptor) { Converter = Converter };
        }
    }
}