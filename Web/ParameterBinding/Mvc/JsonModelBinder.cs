﻿using System.Web.Mvc;
using NiuNai.Bottle.Json;

namespace NiuNai.Bottle.Web.ParameterBinding.Mvc
{
    public class JsonModelBinder : IModelBinder
    {
        public string ParamName { get; set; }

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var parameterName = ParamName;
            if (string.IsNullOrEmpty(parameterName)) parameterName = bindingContext.ModelName;

            var valueResult = bindingContext.ValueProvider.GetValue(parameterName);
            var json = valueResult.RawValue as string;

            if (string.IsNullOrEmpty(json)) return null;

            return JsonUtils.Deserialize(bindingContext.ModelType, json);
        }
    }
}