﻿using System.Web.Mvc;

namespace NiuNai.Bottle.Web.ParameterBinding.Mvc
{
    public class JsonAttribute : CustomModelBinderAttribute
    {
        public JsonAttribute()
            : this(null)
        {
        }

        public JsonAttribute(string paramName)
        {
            ParamName = paramName;
        }

        public string ParamName { get; private set; }

        public override IModelBinder GetBinder()
        {
            return new JsonModelBinder { ParamName = ParamName };
        }
    }
}