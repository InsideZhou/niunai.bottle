﻿using System;

namespace NiuNai.Bottle.Web.Auth
{
    /// <summary>
    ///     如果未声明<see cref="PermissionRequiredAttribute"/>，则已声明的可选权限必须拥有至少一个，否则忽略。
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class PermissionOptionalAttribute : Attribute
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
    }
}