﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace NiuNai.Bottle.Web.Auth
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public abstract class AbstractAuthorizeAttribute : FilterAttribute, IAuthorizationFilter
    {
        public virtual void OnAuthorization(AuthorizationContext ctx)
        {
            if (ctx == null) throw new ArgumentNullException("ctx");

            if (CheckPrivilege(ctx)) return;

            HandleUnauthorizedRequest(ctx);
        }

        protected virtual void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            throw new AuthorizationException();
        }

        protected bool CheckPrivilege(AuthorizationContext ctx)
        {
            if (!ctx.HttpContext.User.Identity.IsAuthenticated) return false;

            var controller = ctx.ActionDescriptor.ControllerDescriptor;
            var action = ctx.ActionDescriptor;

            var requiredRoles = new List<RoleRequiredAttribute>((RoleRequiredAttribute[]) controller.GetCustomAttributes(typeof(RoleRequiredAttribute), false));
            requiredRoles.AddRange((RoleRequiredAttribute[]) action.GetCustomAttributes(typeof(RoleRequiredAttribute), false));

            var requiredPermissions = new List<PermissionRequiredAttribute>((PermissionRequiredAttribute[]) controller.GetCustomAttributes(typeof(PermissionRequiredAttribute), false));
            requiredPermissions.AddRange((PermissionRequiredAttribute[]) action.GetCustomAttributes(typeof(PermissionRequiredAttribute), false));

            var authInfo = GetAuthInfo(ctx);

            if (requiredRoles.Count > 0
                && !(from requiredRole in requiredRoles where !string.IsNullOrEmpty(requiredRole.Code) select requiredRole.Code)
                .All(requiredCode => authInfo.RoleCodes.Contains(requiredCode))) return false;

            if (requiredPermissions.Count > 0
                && !(from requiredPermission in requiredPermissions where !string.IsNullOrEmpty(requiredPermission.Code) select requiredPermission.Code)
                .All(requiredCode => authInfo.PermissionCodes.Contains(requiredCode))) return false;

            var optionalRoles = new List<RoleOptionalAttribute>((RoleOptionalAttribute[]) controller.GetCustomAttributes(typeof(RoleOptionalAttribute), false));
            optionalRoles.AddRange((RoleOptionalAttribute[]) action.GetCustomAttributes(typeof(RoleOptionalAttribute), false));

            var optionalPermissions = new List<PermissionOptionalAttribute>((PermissionOptionalAttribute[]) controller.GetCustomAttributes(typeof(PermissionOptionalAttribute), false));
            optionalPermissions.AddRange((PermissionOptionalAttribute[]) action.GetCustomAttributes(typeof(PermissionOptionalAttribute), false));

            if (requiredRoles.Count == 0 && optionalRoles.Count > 0
                && !(from optionalRole in optionalRoles where !string.IsNullOrEmpty(optionalRole.Code) select optionalRole.Code)
                .All(optionalCode => authInfo.RoleCodes.Contains(optionalCode))) return false;

            if (requiredPermissions.Count == 0 && optionalPermissions.Count > 0
                && !(from optionalPermission in optionalPermissions where !string.IsNullOrEmpty(optionalPermission.Code) select optionalPermission.Code)
                .All(optionalCode => authInfo.PermissionCodes.Contains(optionalCode))) return false;

            return true;
        }

        protected abstract IAuthInfo GetAuthInfo(AuthorizationContext ctx);
    }

    public class AuthorizationException : Exception
    {
    }

    public interface IAuthInfo
    {
        string Pwd { get; }
        List<string> RoleCodes { get; }
        List<string> PermissionCodes { get; }
    }
}