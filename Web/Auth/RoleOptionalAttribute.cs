﻿using System;

namespace NiuNai.Bottle.Web.Auth
{
    /// <summary>
    ///     如果未声明<see cref="RoleRequiredAttribute"/>，则已声明的可选角色必须拥有至少一个，否则忽略。
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class RoleOptionalAttribute : Attribute
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
    }
}