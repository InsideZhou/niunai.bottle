﻿using System;

namespace NiuNai.Bottle.Web.Auth
{
    /// <summary>
    ///     必需角色。
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class RoleRequiredAttribute : Attribute
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
    }
}