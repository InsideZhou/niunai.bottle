﻿using System;

namespace NiuNai.Bottle.Web.Auth
{
    /// <summary>
    ///     必需权限。
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class PermissionRequiredAttribute : Attribute
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
    }
}