﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using NiuNai.Bottle.Web.ActionResult;
using NLog;
using ILogger = NiuNai.Bottle.Log.ILogger;

namespace NiuNai.Bottle.Web
{
    public abstract class AbstractApiController : ApiController, ILogger
    {
        private static readonly bool _IsDebugEnabled = ((CompilationSection) WebConfigurationManager.GetSection(@"system.web/compilation")).Debug;

        public static bool IsDebugEnabled
        {
            get { return _IsDebugEnabled; }
        }

        protected ILogger Logger
        {
            get { return WebLogger.Instance; }
        }

        [NonAction]
        public void Trace(string msg)
        {
            WebLogger.Instance.Log(LogLevel.Trace, msg);
        }

        [NonAction]
        public void Trace(string logger, string msg)
        {
            WebLogger.Instance.Log(LogLevel.Trace, logger, msg);
        }

        [NonAction]
        public void Debug(string msg)
        {
            WebLogger.Instance.Log(LogLevel.Debug, msg);
        }

        [NonAction]
        public void Debug(string logger, string msg)
        {
            WebLogger.Instance.Log(LogLevel.Debug, logger, msg);
        }

        [NonAction]
        public void Info(string msg)
        {
            WebLogger.Instance.Log(LogLevel.Info, msg);
        }

        [NonAction]
        public void Info(string logger, string msg)
        {
            WebLogger.Instance.Log(LogLevel.Info, logger, msg);
        }

        [NonAction]
        public void Warn(string msg)
        {
            WebLogger.Instance.Log(LogLevel.Warn, msg);
        }

        [NonAction]
        public void Warn(string logger, string msg)
        {
            WebLogger.Instance.Log(LogLevel.Warn, logger, msg);
        }

        [NonAction]
        public void Error(string msg)
        {
            WebLogger.Instance.Log(LogLevel.Error, msg);
        }

        [NonAction]
        public void Error(string logger, string msg)
        {
            WebLogger.Instance.Log(LogLevel.Error, logger, msg);
        }

        protected NegotiatedContentResult<T> Ok<T>(T content, JsonSerializerSettings jsonSettings)
        {
            var result = new BottleNegotiatedContentResult<T>(HttpStatusCode.OK, content, this);
            result.JsonMediaTypeFormatter = new JsonMediaTypeFormatter();
            result.JsonMediaTypeFormatter.SerializerSettings = jsonSettings;
            return result;
        }

        protected virtual LogEventInfo GenerateLogEvent()
        {
            var headers = from header in Request.Headers select string.Format("{0}={1}", header.Key, string.Join(";", header.Value));

            var parameters = Request.GetQueryNameValuePairs();

            var evt = LogEventInfo.CreateNullEvent();
            evt.Properties["Url"] = Request.RequestUri;
            evt.Properties["Headers"] = string.Join("&", headers);
            evt.Properties["Params"] = string.Join("&", from p in parameters select string.Format("{0}={1}", p.Key, p.Value));
            return evt;
        }
    }
}