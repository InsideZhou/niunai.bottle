﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace NiuNai.Bottle.Web.ActionResult
{
    public class BottleNegotiatedContentResult<T> : NegotiatedContentResult<T>
    {
        public BottleNegotiatedContentResult(
            HttpStatusCode statusCode,
            T content,
            IContentNegotiator contentNegotiator,
            HttpRequestMessage request,
            IEnumerable<MediaTypeFormatter> formatters
            )
            : base(statusCode, content, contentNegotiator, request, formatters)
        {
        }

        public BottleNegotiatedContentResult(HttpStatusCode statusCode, T content, ApiController controller)
            : base(statusCode, content, controller)
        {
        }

        public JsonMediaTypeFormatter JsonMediaTypeFormatter { get; set; }

        public override Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            if (null == JsonMediaTypeFormatter) return base.ExecuteAsync(cancellationToken);

            var negotiationResult = ContentNegotiator.Negotiate(typeof(T), Request, Formatters);
            var httpResponseMessage = new HttpResponseMessage();
            try
            {
                if (negotiationResult == null)
                {
                    httpResponseMessage.StatusCode = HttpStatusCode.NotAcceptable;
                }
                else
                {
                    httpResponseMessage.StatusCode = StatusCode;

                    var finalFormatter = negotiationResult.Formatter;
                    if (negotiationResult.Formatter is JsonMediaTypeFormatter)
                    {
                        finalFormatter = JsonMediaTypeFormatter;
                    }

                    httpResponseMessage.Content = new ObjectContent<T>(Content, finalFormatter, negotiationResult.MediaType);
                }

                httpResponseMessage.RequestMessage = Request;
            }
            catch
            {
                httpResponseMessage.Dispose();
                throw;
            }

            return Task.FromResult(httpResponseMessage);
        }
    }
}