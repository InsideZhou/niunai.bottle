﻿using System;
using NLog;

namespace NiuNai.Bottle.Web
{
    public class WebLogger : Log.ILogger
    {
        public static string RootLogger = "ROOT";

        private readonly static WebLogger Logger = new WebLogger();

        public static WebLogger Instance
        {
            get
            {
                return Logger;
            }
        }

        WebLogger()
        {
        }

        public void Log(LogLevel level, string msg, Exception e = null, LogEventInfo eventInfo = null)
        {
            Log(level, RootLogger, msg, e, eventInfo);
        }

        public void Log(LogLevel level, string logger, string msg, Exception e = null, LogEventInfo eventInfo = null)
        {
            if (null == eventInfo)
            {
                LogManager.GetLogger(logger).Log(level, msg);
            }
            else
            {
                eventInfo.Message = msg;
                eventInfo.Level = level;
                eventInfo.LoggerName = logger;
                eventInfo.Exception = e;
                LogManager.GetLogger(logger).Log(eventInfo);
            }
        }

        public void Trace(string msg)
        {
            Log(LogLevel.Trace, RootLogger, msg);
        }

        public void Trace(string logger, string msg)
        {
            Log(LogLevel.Trace, logger, msg);
        }

        public void Debug(string msg)
        {
            Log(LogLevel.Debug, RootLogger, msg);
        }

        public void Debug(string logger, string msg)
        {
            Log(LogLevel.Debug, logger, msg);
        }

        public void Info(string msg)
        {
            Log(LogLevel.Info, RootLogger, msg);
        }

        public void Info(string logger, string msg)
        {
            Log(LogLevel.Info, logger, msg);
        }

        public void Warn(string msg)
        {
            Log(LogLevel.Warn, RootLogger, msg);
        }

        public void Warn(string logger, string msg)
        {
            Log(LogLevel.Warn, logger, msg);
        }

        public void Error(string msg)
        {
            Log(LogLevel.Error, RootLogger, msg);
        }

        public void Error(string logger, string msg)
        {
            Log(LogLevel.Error, logger, msg);
        }
    }
}