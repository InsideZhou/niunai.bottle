﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Web;

namespace NiuNai.Bottle.Web
{
    public static class UriExtension
    {
        public static Uri AddQueryParam(this Uri origin, string key, string value)
        {
            if (null == origin || string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException();
            }

            var ub = new UriBuilder(origin);
            var nv = HttpUtility.ParseQueryString(origin.Query);
            nv.Add(key, value);
            ub.Query = _NameValueCollectionToQuery(nv);

            return ub.Uri;
        }

        public static Uri ClearQuery(this Uri origin)
        {
            if (null == origin)
            {
                throw new ArgumentNullException();
            }

            if (string.IsNullOrEmpty(origin.Query))
            {
                return origin;
            }
            var ub = new UriBuilder(origin);
            ub.Query = string.Empty;
            return ub.Uri;
        }

        public static Uri DeleteQueryParam(this Uri origin, string key)
        {
            if (null == origin || string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException();
            }

            var ub = new UriBuilder(origin);
            var nv = HttpUtility.ParseQueryString(ub.Query);
            nv.Remove(key);
            ub.Query = _NameValueCollectionToQuery(nv);

            return ub.Uri;
        }

        public static Uri SetQuery(this Uri origin, NameValueCollection nv)
        {
            if (null == origin || null == nv)
            {
                throw new ArgumentNullException();
            }

            if (nv.Count > 0)
            {
                var ub = new UriBuilder(origin);
                ub.Query = _NameValueCollectionToQuery(nv);
                return ub.Uri;
            }
            return origin;
        }

        public static Uri SetQueryParam(this Uri origin, string key, string value)
        {
            if (null == origin || string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException();
            }

            var ub = new UriBuilder(origin);
            var nv = HttpUtility.ParseQueryString(origin.Query);
            nv[key] = value;
            ub.Query = _NameValueCollectionToQuery(nv);

            return ub.Uri;
        }

        private static string _NameValueCollectionToQuery(NameValueCollection nv)
        {
            if (null == nv || nv.Count <= 0)
            {
                return null;
            }

            var query = new StringBuilder();
            foreach (string key in nv)
            {
                query.AppendFormat("{0}={1}&", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(nv[key]));
            }
            query.Remove(query.Length - 1, 1);

            return query.ToString();
        }
    }
}