﻿using System.Collections.Specialized;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;

namespace NiuNai.Bottle.Web
{
    public static class HttpRequestBaseExtension
    {
        public static NameValueCollection RequestParams(this HttpRequestBase src)
        {
            var result = new NameValueCollection();
            result.Add(src.QueryString);
            result.Add(src.Form);
            return result;
        }

        public static string RealIp(this HttpRequestBase src)
        {
            var realipHeaderKey = ConfigurationManager.AppSettings["X-Real-IP"];
            var realip = src.Headers[string.IsNullOrEmpty(realipHeaderKey) ? "X-Real-IP" : realipHeaderKey];
            return string.IsNullOrEmpty(realip) ? src.UserHostAddress : realip;
        }

        public static bool IsMobileAgent(this HttpRequestBase src)
        {
            if (null == src.UserAgent) return false;

            return IsWechatAgent(src) ||
                Regex.IsMatch(src.UserAgent, @"mobile", RegexOptions.IgnoreCase) ||
                src.Browser.IsMobileDevice;
        }

        public static bool IsWechatAgent(this HttpRequestBase src)
        {
            return null != src.UserAgent && src.UserAgent.ToUpper().Contains("MICROMESSENGER");
        }
    }
}