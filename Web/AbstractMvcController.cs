﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Compilation;
using System.Web.Configuration;
using System.Web.Mvc;
using NiuNai.Bottle.Json;
using NiuNai.Bottle.Web.Auth;
using NLog;
using ILogger = NiuNai.Bottle.Log.ILogger;

namespace NiuNai.Bottle.Web
{
    public abstract class AbstractMvcController : Controller, ILogger
    {
        private static readonly bool _IsDebugEnabled = ((CompilationSection) WebConfigurationManager.GetSection(@"system.web/compilation")).Debug;

        public static bool IsDebugEnabled
        {
            get { return _IsDebugEnabled; }
        }

        public static List<Type> MvcControllers
        {
            get
            {
                var types = new List<Type>();

                foreach (Assembly assembly in BuildManager.GetReferencedAssemblies())
                {
                    foreach (var t in assembly.GetTypes())
                    {
                        if (t != null
                            && t.IsPublic
                            && t.Name.EndsWith("Controller", StringComparison.OrdinalIgnoreCase)
                            && !t.IsAbstract
                            && typeof(IController).IsAssignableFrom(t))
                        {
                            types.Add(t);
                        }
                    }
                }

                return types;
            }
        }

        public static List<MethodInfo> MvcActions
        {
            get
            {
                var methods = new List<MethodInfo>();
                foreach (var c in MvcControllers)
                {
                    methods.AddRange(
                        c.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly |
                                     BindingFlags.InvokeMethod)
                            .Where(m =>
                                !m.IsDefined(typeof(NonActionAttribute), false)
                                && (m.ReturnType == typeof(System.Web.Mvc.ActionResult) || m.ReturnType.IsInstanceOfType(typeof(System.Web.Mvc.ActionResult)))
                            ));
                }

                return methods;
            }
        }

        protected List<Type> Controllers
        {
            get { return MvcControllers; }
        }

        protected List<MethodInfo> Actions
        {
            get { return MvcActions; }
        }

        [NonAction]
        public void Trace(string msg)
        {
            WebLogger.Instance.Log(LogLevel.Trace, msg);
        }

        [NonAction]
        public void Trace(string logger, string msg)
        {
            WebLogger.Instance.Log(LogLevel.Trace, logger, msg);
        }

        [NonAction]
        public void Debug(string msg)
        {
            WebLogger.Instance.Log(LogLevel.Debug, msg);
        }

        [NonAction]
        public void Debug(string logger, string msg)
        {
            WebLogger.Instance.Log(LogLevel.Debug, logger, msg);
        }

        [NonAction]
        public void Info(string msg)
        {
            WebLogger.Instance.Log(LogLevel.Info, msg);
        }

        [NonAction]
        public void Info(string logger, string msg)
        {
            WebLogger.Instance.Log(LogLevel.Info, logger, msg);
        }

        [NonAction]
        public void Warn(string msg)
        {
            WebLogger.Instance.Log(LogLevel.Warn, msg);
        }

        [NonAction]
        public void Warn(string logger, string msg)
        {
            WebLogger.Instance.Log(LogLevel.Warn, logger, msg);
        }

        [NonAction]
        public void Error(string msg)
        {
            WebLogger.Instance.Log(LogLevel.Error, msg);
        }

        [NonAction]
        public void Error(string logger, string msg)
        {
            WebLogger.Instance.Log(LogLevel.Error, logger, msg);
        }

        protected new System.Web.Mvc.ActionResult Json(object result)
        {
            return Json(result, "application/json", Encoding.UTF8);
        }

        protected new System.Web.Mvc.ActionResult Json(object result, string contentType)
        {
            return Json(result, contentType, Encoding.UTF8);
        }

        protected new System.Web.Mvc.ActionResult Json(object result, string contentType, Encoding encoding)
        {
            return Content(JsonUtils.Serialize(result), contentType, encoding);
        }

        protected string ViewAsString(string viewName, object model)
        {
            return ViewAsString(viewName, null, model);
        }

        protected string ViewAsString(string viewName, string masterName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = RouteData.GetRequiredString("action");
            }

            ViewData.Model = model;

            var viewEngineResult = ViewEngines.Engines.FindView(ControllerContext, viewName, masterName);
            var view = viewEngineResult.View;
            var sb = new StringBuilder();
            using (var writer = new StringWriter(sb))
            {
                view.Render(new ViewContext(ControllerContext, view, ViewData, TempData, writer), writer);
                viewEngineResult.ViewEngine.ReleaseView(ControllerContext, view);
            }

            return sb.ToString();
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            if (Response.StatusCode == (int) HttpStatusCode.Unauthorized)
            {
                //提前结束响应，避免401被asp.net的forms验证替换为302。
                Response.End();
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);

            var isAjaxRequest = filterContext.HttpContext.Request.IsAjaxRequest();

            if (filterContext.Exception is AuthenticationException)
            {
                filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                if (isAjaxRequest)
                {
                    filterContext.HttpContext.Response.AppendHeader("X-Responsed-With", "Authentication Fail");
                }

                filterContext.ExceptionHandled = true;
            }
            else if (filterContext.Exception is AuthorizationException)
            {
                filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Forbidden);
                if (isAjaxRequest)
                {
                    filterContext.HttpContext.Response.AppendHeader("X-Responsed-With", "Authorization Fail");
                }

                filterContext.ExceptionHandled = true;
            }
            else if (!(filterContext.Exception is HttpException))
            {
                WebLogger.Instance.Log(LogLevel.Error, string.Format("Exception.{0}", GetType().FullName), filterContext.Exception.Message, filterContext.Exception, GenerateLogEvent());
            }
        }

        protected LogEventInfo GenerateLogEvent()
        {
            var debug = IsDebugEnabled;
            var headers = from key in Request.Headers.AllKeys select string.Format("{0}={1}", key, Request.Headers[key]);

            var parameters = new NameValueCollection();
            parameters.Add(Request.QueryString);
            parameters.Add(Request.Form);

            var evt = LogEventInfo.CreateNullEvent();
            evt.Properties["Debug"] = debug;
            evt.Properties["Url"] = Request.Url;
            evt.Properties["Headers"] = string.Join("&", headers);
            evt.Properties["Params"] = string.Join("&", from key in parameters.AllKeys select string.Format("{0}={1}", key, parameters[key]));
            return evt;
        }
    }

    public class AuthenticationException : Exception
    {
    }
}