﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;
using NiuNai.Bottle.Web.Auth;

namespace NiuNai.Bottle.Web.MessageHandler
{
    public class BottleExceptionHandler : ExceptionHandler
    {
        private static readonly bool _IsDebugEnabled = ((CompilationSection) WebConfigurationManager.GetSection(@"system.web/compilation")).Debug;

        public static bool IsDebugEnabled
        {
            get { return _IsDebugEnabled; }
        }

        public override Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
        {
            return Task.Run(() => Handle(context), cancellationToken);
        }

        public override void Handle(ExceptionHandlerContext context)
        {
            var e = context.Exception;
            var httpError = new HttpError(e.Message);

            if (IsDebugEnabled)
            {
                httpError.ExceptionType = e.GetType().FullName;
                httpError.StackTrace = e.StackTrace;
            }

            if (e is AuthenticationException)
            {
                context.Result = new ResponseMessageResult(context.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, httpError));
                return;
            }

            if (e is AuthorizationException)
            {
                context.Result = new ResponseMessageResult(context.Request.CreateErrorResponse(HttpStatusCode.Forbidden, httpError));
                return;
            }

            context.Result = new ResponseMessageResult(context.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, httpError));
        }

        public override bool ShouldHandle(ExceptionHandlerContext context)
        {
            return context.CatchBlock != ExceptionCatchBlocks.IExceptionFilter && base.ShouldHandle(context);
        }
    }
}