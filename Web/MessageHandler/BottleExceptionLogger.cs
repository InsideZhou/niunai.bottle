﻿using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using NiuNai.Bottle.Web.Auth;
using NLog;

namespace NiuNai.Bottle.Web.MessageHandler
{
    public class BottleExceptionLogger : ExceptionLogger
    {
        public override Task LogAsync(ExceptionLoggerContext context, CancellationToken cancellationToken)
        {
            return Task.Run(() => Log(context), cancellationToken);
        }

        public override void Log(ExceptionLoggerContext context)
        {
            var e = context.Exception;
            WebLogger.Instance.Log(LogLevel.Error, string.Format("Exception.{0}", GetType().FullName), e.Message, e, GenerateLogEvent(context.Request));
        }

        public virtual LogEventInfo GenerateLogEvent(HttpRequestMessage request)
        {
            var headers = from header in request.Headers select string.Format("{0}={1}", header.Key, string.Join(";", header.Value));

            var parameters = request.GetQueryNameValuePairs();

            var evt = LogEventInfo.CreateNullEvent();
            evt.Properties["Url"] = request.RequestUri;
            evt.Properties["Headers"] = string.Join("&", headers);
            evt.Properties["Params"] = string.Join("&", from p in parameters select string.Format("{0}={1}", p.Key, p.Value));
            return evt;
        }

        public override bool ShouldLog(ExceptionLoggerContext context)
        {
            var e = context.Exception;
            return !(e is AuthenticationException || e is AuthorizationException || e is HttpException || e is HttpResponseException);
        }
    }
}
