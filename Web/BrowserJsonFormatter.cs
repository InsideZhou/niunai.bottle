﻿using System;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;

namespace NiuNai.Bottle.Web
{
    public class BrowserJsonFormatter : JsonMediaTypeFormatter
    {
        public BrowserJsonFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }

        public BrowserJsonFormatter(JsonMediaTypeFormatter formatter)
            : base(formatter)
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }

        public override void SetDefaultContentHeaders(Type type, HttpContentHeaders headers, MediaTypeHeaderValue mediaType)
        {
            base.SetDefaultContentHeaders(type, headers, new MediaTypeHeaderValue("application/json"));
        }
    }
}