﻿using System.Web.Mvc;
using NiuNai.Bottle.Web;

namespace Web.Test.Controllers
{
    public class HomeController : AbstractMvcController
    {
        public ActionResult Index()
        {
            return View();
        }

    }
}
